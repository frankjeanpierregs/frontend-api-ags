const contenido = document.querySelector('#pedidos');

let url = '';

function buscar() {
    console.log(url, 'asda');
    url = document.querySelector('#iBuscar').value;
    getConnecion()
};



let URLbase = 'http://localhost:8080/pedido/';

const getConnecion  = () => {
    URLbase = 'http://localhost:8080/pedido/';
    URLbase += url;
    var myHeaders = new Headers();
    var miInit = { method: 'GET',
               headers: myHeaders,
               mode: 'cors',
               cache: 'default' };

    fetch(URLbase, miInit)
    .then(response => {
        if(response.status === 200) {
            response.json()
            .then(response => {
                if(response.length) {
                    contenido.innerHTML = '';
                    response.forEach(pedido => {
                        crearPedido(pedido);
                    });
                } else {
                    contenido.innerHTML = '';
                    crearPedido(response);
                }
            })
        } else {
            alert('Error en la petición')
        }
    });
    url = '';
};

// getProveedores();
getConnecion();

function crearPedido(p) {
    let html = `<div class="informacion">
            <h5>Pedido:</h5>
            <span>${p.codigo}</span>
            <span>${p.fechaCreacion}</span>
            <span>${p.fechaEntrega}</span>
            <div class="estado">
                <i class="fas fa-3x fa-check" id="disponible"></i>`
                if (p.pedidoCliente) {
                    html += `<label for="" id="descripcion">${p.pedidoCliente.pedidoCliente_pk}</label></div>`;
                    html += `<p>${p.pedidoProveedor.pedidoProveedor_pk}</p>
                    </div>`; 
                } else {
                    html += `<label for="" id="descripcion">Sin asignar</label></div>`;
                    html += `<p>Sin asignar</p>
                    </div>`; 
                }
            
        contenido.innerHTML += html;
}


